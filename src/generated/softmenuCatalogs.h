/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
* \file softmenuCatalogs.h
***********************************************/

/***********************************************************************************************
* Do not edit this file manually! It's automagically generated by the program generateCatalogs *
************************************************************************************************/

#if !defined(SOFTMENUCATALOGS_H)
  #define SOFTMENUCATALOGS_H

  /*<--------- 6 functions --------->*/
  /*<---- 6 f shifted functions ---->*/
  /*<---- 6 g shifted functions ---->*/
  #if defined(DMCP_BUILD)
    TO_QSPI const int16_t menu_FCNS[] = {
       1881,  220,  221,   67, 1836, 1404,
         73, 1406,   64, 1837, 1824, 1823,
       1825,   62, 1838, 1835, 1407, 1748,
        234,  238, 1408, 1409, 1410, 1798,
        124,   81,   82,   83,   85,   84,
         86, 1749,  416, 1411, 1750, 1775,
        243, 1814,  244, 1412,  247, 1413,
        364,  406, 1414, 1415, 1297, 1435,
       1831, 1208, 1209, 1210, 1211, 1416,
       1417,  405,  248, 2004,  250, 1418,
       1304, 2055, 1213, 1214, 1215, 1216,
        407,   87,  110,  376,   97, 1419,
       1780, 1420, 1421, 2033, 1423, 1424,
       1425, 1426, 1427, 1428,   41, 1429,
        207, 1683,   49, 1848, 1431,   56,
       1433,   74,   75, 1434,   26, 1436,
        350,  380,  256, 1437, 1438, 1439,
       1440, 1441, 1442, 1443,  225,  222,
         91, 1833, 1444, 1445, 1446, 1455,
       1448, 1751, 1453, 1684, 1449, 1873,
         37, 1544,    8,    9, 1573, 1450,
         10, 1862, 1451, 1452, 1744, 1693,
       1454,  120, 1456, 1457, 1458, 1459,
       1460, 1461,   35,   57, 1465, 1466,
       1467, 1853, 1468,   22,   65, 2060,
       1469, 1298, 2049, 1218, 1219, 1220,
       1221, 1470, 1575, 1727, 1764, 1816,
        409, 1722,   20,  396,  398,  397,
        386,  112, 1795, 1472,   42, 1473,
       1474, 1935,   88,  358,   94, 1223,
         24,  231,   21,  399,  401,  400,
        258,  260,  266,  270, 1763, 1224,
       1225, 1226, 1475, 1476,  274,  276,
       1477, 2054, 1303,   89, 1478, 1479,
       1228, 1229, 1230, 1231, 1480, 1481,
          2, 1482, 1797,  353,  294,  316,
        236,  240,  370, 1834, 1757, 1766,
       1790, 1791, 1483, 1793, 1484, 1854,
       1839,  278, 1792,  280,  282, 1233,
       1234, 1235, 1236, 1306, 2057,  100,
       1632,  284, 1485,   92, 1486,   43,
         25, 1752,  288,   93,    5,    6,
       1606,    7, 1487, 1488, 1489, 1754,
       1755, 1490, 1491,  352, 1492, 1493,
       1494, 1495, 1471,  249,  251, 1863,
        291, 1497, 1498, 1499,   77,  257,
        355,  292,  311,  296,  300,  304,
        310,  389,  391,  329,  330, 1501,
       1726, 1865, 1502,   68,  252,  320,
          1, 1503,  293,   90, 1504, 1238,
       1239, 1240, 1241,  314, 1299, 2050,
       2083, 1880,  374,  417, 1505, 1506,
         69, 1507, 1508, 1614, 1509, 1510,
       1511, 1512, 1552, 1513, 1514, 1515,
         71, 1300, 2051, 1243, 1244, 1245,
       1246,   72, 1516,  322,  275,  277,
        357,  371,  373,  366, 1517,  419,
        420,   27, 1518,  103, 1528, 1519,
       1520,  388, 1840,  104,  421,  328,
        336,  268,  272,  324,  289,  335,
        102, 1521, 1522, 1523, 1707,  372,
       1524, 1796, 1689, 1525, 1526, 1527,
       1529, 1530, 1531, 1532, 1533, 1534,
       1535, 1536, 1537, 1538, 1646, 1539,
        113, 1541,  245,  377,  381,  387,
        361,  259,  263,  375,  323,  339,
        363,  332,  340,  379,  384, 1932,
        402,   28, 1248, 1249, 1250, 1251,
        106,  107,  390,  331,  360,  254,
       1542,  403, 1253, 1254, 1255, 1256,
        123,  435,  321, 1832,   23, 1543,
       1827, 1828, 1829, 1830,  125, 2052,
       1301,  295, 2056, 1305,   38,  242,
        246,  286,  326,  343,  344,  333,
         50, 1546, 1547, 1548, 1549, 2040,
       2042, 1550, 1551, 1258, 1259, 1260,
       1261, 1553, 1302, 2053, 1554,   33,
       2039,  228,  342, 1555,  337, 1556,
        356, 1557, 1558, 1677, 1678, 1675,
       1559,   51, 1822, 1820, 1818, 1561,
       1562, 1563, 1564,   52,   53,   54,
         55, 1432, 1462, 1565, 1566, 1567,
         29, 1560, 1568, 2059, 1569, 1570,
        418,  410, 2001,  411, 1571,  122,
       2018, 2019, 1572, 1574, 1307, 2058,
       1868,   66, 1576, 1869,  412, 2002,
        413, 1577, 1578,    4, 1579, 1580,
       1581, 1582, 1583,  121, 1690,   39,
         40, 1585, 1586,  408, 1587,  298,
       1588,  423,  424, 1841, 1589, 1591,
       1592, 1593, 1594, 1595, 1596, 1597,
       1598, 1599,  111, 1742, 1866, 1600,
       1601, 1602,   76, 1500, 1540,   78,
       1603,  414, 1999, 1604, 1605, 1758,
       1607, 1405, 1682, 1608,   30, 1768,
        415, 2000, 1609, 1610,   44, 1821,
       1819, 1817, 1611, 1612, 1613,   70,
       1622, 1615,   45,   46,   47,   48,
        302, 1430, 1545,   31, 1617, 1618,
       1815, 1743, 1447, 2038,  307,  348,
         79,   80, 1619, 1620, 1621, 1843,
       1623, 1624,  313,   34,  346, 1263,
       1753,  318, 1264, 1265, 1266, 1625,
       1626, 1627, 1723, 1867, 1628, 1629,
       1630, 1631,  101, 1745, 1633, 1268,
       1269, 1270, 1271, 1634,  290, 1635,
       1636, 1590, 1638, 1639, 1637, 2003,
        279,  281,  283, 1643, 1640, 1826,
         58,   59,    3, 1965, 1966, 1967,
       1968, 1969, 1970, 1971, 1972, 1973,
       1974, 1975, 1976, 1977, 1978, 1979,
       1980, 1981, 1982, 1641, 1746, 2078,
       2077, 1653, 2074, 1654,  404,  126,
       2075, 2076, 2079, 1747, 1616, 1642,
         63, 1957, 1912,  108, 1644, 1645,
        127,   36,   16,   17,   11,   15,
         12,   18,   19,   13,   14, 2082,
       1648, 1812,  341, 1647,  349,  378,
       1649,   60, 1665, 1650,  382, 1651,
       1652, 1655, 1656, 1657, 1658, 1659,
       1660, 1661, 1663, 1662, 1664, 1813,
       1666, 1667, 1668, 1669, 1670, 1765,
       1671, 1728, 1673,  452,  453,  455,
        456,  444,  447,  443,  442,  446,
        450, 1672, 1674,  436,  438,  449,
        439,  451,  457,  458,  448,  441,
        454,  437,  440,  445,  433,  434,
       1278, 1279, 1280, 1281, 1273, 1274,
       1275, 1584, 1276, 1679, 1704, 1705,
         95,   32,   96,   98, 1680, 1701,
       1909, 1910,   99, 1859, 1681, 1959,
        115,  116, 1960,  117, 1961, 1685,
       1964, 1686, 1872, 1962,  118, 1849,
        119, 1963, 1691, 1850, 1842, 1789,
       1788, 1694, 1703, 1702,  105, 1695,
       1696, 1697, 1698, 1699,   61, 1794,
       1700, 1706, 1708, 1799, 1709, 1710,
       1711, 1712, 1713, 1714, 1715, 1716,
       1717, 1718, 1719, 1676, 1720, 1721,
        422,
    };
  #else // !DMCP_BUILD
    TO_QSPI const int16_t menu_FCNS[] = {
       1881,  220,  221,   67, 1836, 1404,
         73, 1406,   64, 1837, 1824, 1823,
       1825,   62, 1838, 1835, 1407, 1748,
        234,  238, 1408, 1409, 1410, 1798,
        124,   81,   82,   83,   85,   84,
         86, 1749,  416, 1411, 1750, 1775,
        243, 1814,  244, 1412,  247, 1413,
        364,  406, 1414, 1415, 1297, 1435,
       1831, 1208, 1209, 1210, 1211, 1416,
       1417,  405,  248, 2004,  250, 1418,
       1304, 2055, 1213, 1214, 1215, 1216,
        407,   87,  110,  376,   97, 1419,
       1780, 1420, 1421, 2033, 1423, 1424,
       1425, 1426, 1427, 1428,   41, 1429,
        207, 1683,   49, 1848, 1431,   56,
       1433,   74,   75, 1434,   26, 1436,
        350,  380,  256, 1437, 1438, 1439,
       1440, 1441, 1442, 1443,  225,  222,
         91, 1833, 1444, 1445, 1446, 1455,
       1448, 1751, 1453, 1684, 1449, 1873,
         37, 1544,    8,    9, 1573, 1450,
         10, 1862, 1451, 1452, 1744, 1693,
       1454,  120, 1456, 1457, 1458, 1459,
       1460, 1461,   35,   57, 1465, 1466,
       1467, 1853, 1468,   22,   65, 2060,
       1469, 1298, 2049, 1218, 1219, 1220,
       1221, 1470, 1575, 1727, 1764, 1816,
        409, 1722,   20,  396,  398,  397,
        386,  112, 1795, 1472,   42, 1473,
       1474, 1935,   88,  358,   94, 1223,
         24,  231,   21,  399,  401,  400,
        258,  260,  266,  270, 1763, 1224,
       1225, 1226, 1475, 1476,  274,  276,
       1477, 2054, 1303,   89, 1478, 1479,
       1228, 1229, 1230, 1231, 1480, 1481,
          2, 1482, 1797,  353,  294,  316,
        236,  240,  370, 1834, 1757, 1766,
       1790, 1791, 1483, 1793, 1484, 1854,
       1839,  278, 1792,  280,  282, 1233,
       1234, 1235, 1236, 1306, 2057,  100,
       1632,  284, 1485,   92, 1486,   43,
         25, 1752,  288,   93,    5,    6,
       1606,    7, 1487, 1488, 1489, 1754,
       1755, 1490, 1491,  352, 1492, 1493,
       1494, 1495, 1471,  249,  251, 1863,
        291, 1497, 1498, 1499,   77,  257,
        355,  292,  311,  296,  300,  304,
        310,  389,  391,  329,  330, 1501,
       1726, 1865, 1502,   68,  252,  320,
          1, 1503,  293,   90, 1504, 1238,
       1239, 1240, 1241,  314, 1299, 2050,
       2083, 1880,  374,  417, 1505, 1506,
         69, 1507, 1508, 1614, 1509, 1510,
       1511, 1512, 1552, 1513, 1514, 1515,
         71, 1300, 2051, 1243, 1244, 1245,
       1246,   72, 1516,  322,  275,  277,
        357,  371,  373,  366, 1517,  419,
        420,   27, 1518,  103, 1528, 1519,
       1520,  388, 1840,  104,  421,  328,
        336,  268,  272,  324,  289,  335,
        102, 1521, 1522, 1523, 1707,  372,
       1524, 1796, 1689, 1525, 1526, 1527,
       1529, 1530, 1531, 1532, 1533, 1534,
       1535, 1536, 1537, 1538, 1646, 1539,
        113, 1541,  245,  377,  381,  387,
        361,  259,  263,  375,  323,  339,
        363,  332,  340,  379,  384, 1932,
        402,   28, 1248, 1249, 1250, 1251,
        106,  107,  390,  331,  360,  254,
       1542,  403, 1253, 1254, 1255, 1256,
        123,  435,  321, 1832,   23, 1543,
       1827, 1828, 1829, 1830,  125, 2052,
       1301,  295, 2056, 1305,   38,  242,
        246,  286,  326,  343,  344,  333,
         50, 1546, 1547, 1548, 1549, 2040,
       2042, 1550, 1551, 1258, 1259, 1260,
       1261, 1553, 1302, 2053, 1554,   33,
       2039,  228,  342, 1555,  337, 1556,
        356, 1557, 1558, 1677, 1678, 1675,
       1559,   51, 1822, 1820, 1818, 1561,
       1562, 1563, 1564,   52,   53,   54,
         55, 1432, 1462, 1565, 1566, 1567,
         29, 1560, 1568, 2059, 1569, 1570,
        418,  410, 2001,  411, 1571,  122,
       2018, 2019, 1572, 1574, 1307, 2058,
       1868,   66, 1869, 1576,  412, 2002,
        413, 1577, 1578,    4, 1579, 1580,
       1581, 1582, 1583,  121, 1690,   39,
         40, 1585, 1586,  408, 1587,  298,
       1588,  423,  424, 1841, 1589, 1591,
       1593, 1594, 1595, 1596, 1598, 1599,
        111, 1742, 1866, 1600, 1601, 1602,
         76, 1500, 1540,   78, 1603,  414,
       1999, 1604, 1605, 1758, 1607, 1405,
       1682, 1608,   30, 1768,  415, 2000,
       1609, 1610,   44, 1821, 1819, 1817,
       1611, 1612, 1613,   70, 1622, 1615,
         45,   46,   47,   48,  302, 1430,
       1545,   31, 1617, 1618, 1815, 1447,
       2038,  307,  348,   79,   80, 1619,
       1620, 1621, 1843, 1623, 1624,  313,
         34,  346, 1263, 1753,  318, 1264,
       1265, 1266, 1625, 1626, 1627, 1723,
       1867, 1628, 1629, 1630, 1631,  101,
       1745, 1633, 1268, 1269, 1270, 1271,
       1634,  290, 1635, 1636, 1590, 1638,
       1639, 1637, 2003,  279,  281,  283,
       1643, 1640, 1826,   58,   59,    3,
       1965, 1966, 1967, 1968, 1969, 1970,
       1971, 1972, 1973, 1974, 1975, 1976,
       1977, 1978, 1979, 1980, 1981, 1982,
       1641, 1746, 2078, 2077, 1653, 2074,
       1654,  404,  126, 2075, 2076, 2079,
       1747, 1616, 1642,   63, 1957, 1912,
        108, 1644, 1645,  127,   36,   16,
         17,   11,   15,   12,   18,   19,
         13,   14, 2082, 1648, 1812,  341,
       1647,  349,  378, 1649,   60, 1665,
       1650,  382, 1651, 1652, 1655, 1656,
       1657, 1658, 1659, 1660, 1661, 1663,
       1662, 1664, 1813, 1666, 1667, 1668,
       1669, 1670, 1765, 1671, 1728, 1673,
        452,  453,  455,  456,  444,  447,
        443,  442,  446,  450, 1672, 1674,
        436,  438,  449,  439,  451,  457,
        458,  448,  441,  454,  437,  440,
        445,  433,  434, 1278, 1279, 1280,
       1281, 1273, 1274, 1275, 1584, 1276,
       1679, 1704, 1705,   95,   32,   96,
         98, 1680, 1701, 1909, 1910,   99,
       1859, 1681, 1959,  115,  116, 1960,
        117, 1961, 1685, 1964, 1686, 1872,
       1962,  118, 1849, 1963,  119, 1691,
       1850, 1842, 1789, 1788, 1694, 1703,
       1702,  105, 1695, 1696, 1697, 1698,
       1699,   61, 1794, 1700, 1706, 1708,
       1799, 1709, 1710, 1711, 1712, 1713,
       1714, 1715, 1716, 1717, 1718, 1719,
       1676, 1720, 1721,  422,
    };
  #endif // DMCP_BUILD
  TO_QSPI const int16_t menu_CONST[] = {
  128,  129,  130,  131,  132,  133,
  134,  135,  136,  137,  138,  139,
  140,  141,  142,  143,  144,  145,
  146,  147,  148,  149,  150,  151,
  152,  153,  154,  155,  156,  157,
  158,  159,  160,  161,  162,  163,
  164,  165,  166,  167,  168,  169,
  170,  171,  172,  173,  174,  175,
  176,  177,  178,  179,  180,  181,
  182,  183,  184,  185,  186,  187,
  188,  189,  190,  191,  192,  193,
  194,  195,  196,  197,  198,  199,
  200,  201,  202,  203,  204,  205,

  };
  TO_QSPI const int16_t menu_SYSFL[] = {
  491,  477,  497,  498,  493,  494,
  474,  468,  467,  489,  472,  473,
  465,  504,  470,  505,  492,  499,
  500,  476,  484,  466,  490,  496,
  475,  469,  481,  471,  495,  488,
  480,  479,  485,  501,  486,  487,
  463,  482,  503,  483,  502,  464,
  478,
  };
  TO_QSPI const int16_t menu_alpha_INTL[] = {
  550,  667,  665,  670,  669,  668,
  671,  672,  664,  666,  673,  551,
  552,  676,  674,  675,  553,  678,
  677,  554,  682,  680,  684,  683,
  679,  681,  858,  685,  860,  555,
  556,  686,  557,  558,  690,  688,
  692,  691,  687,  689,  693,  694,
  559,  560,  561,  697,  698,  696,
  562,  563,  701,  699,  700,  564,
  705,  703,  708,  707,  706,  709,
  702,  704,  710,  565,  566,  567,
  862,  863,  568,  712,  714,  713,
  711,  569,  716,  715,  570,  720,
  718,  723,  721,  722,  717,  719,
  724,  864,  571,  572,  725,  573,
  574,  727,  726,  728,  575,  729,
  731,  730,
  };
  TO_QSPI const int16_t menu_alpha_intl[] = {
  576,  735,  733,  738,  737,  736,
  739,  740,  732,  734,  741,  577,
  578,  744,  742,  743,  579,  746,
  745,  580,  750,  748,  752,  751,
  747,  749,  859,  753,  861,  581,
  582,  754,  583,  584,  759,  757,
  761,  760,  756,  758,  762,  764,
  585,  586,  587,  766,  767,  765,
  588,  589,  770,  768,  769,  590,
  774,  772,  777,  776,  775,  778,
  771,  773,  779,  591,  592,  593,
  781,  780,  594,  783,  785,  784,
  782,  595,  787,  786,  596,  791,
  789,  794,  792,  793,  788,  790,
  795,  865,  597,  598,  796,  599,
  600,  801,  800,  802,  601,  803,
  805,  804,
  };
#endif // !SOFTMENUCATALOGS_H
